
Date: 04 Nov 2020  16:53:29
Comment:  

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM284
Found: PAM284

FEM284: noise source data:
FEM284: 50.000MHz:29.66dB,70.000MHz:29.78dB,100.000MHz:29.92dB,150.000MHz:30.11dB,200.000MHz:30.19dB
FEM284: temp sensor type Si7021, Mk3 FEM
FEM284: sensor ID: 3863575549883842559 (0x359e2d7e15b5ffff)
FEM284: initial switch (GPIO) state: 0b11111
FEM284: set switch (GPIO) state to load: 0b11000, Success
FEM284: set switch (GPIO) state to noise: 0b11001, Success
FEM284: set switch (GPIO) state to antenna: 0b11111, Success
FEM284: temperature: 35.52C
FEM284: humidity: 19.88%
FEM284: supply sensor: 7.308V, 455.2mA, OK
FEM284: position sensor: theta=91.988, phi=39.324
FEM284: barometer: pressure=1033.3hPa, altitude=-175.7m

PAM284: ROM ID: 8083509848479629313 (0x702e656101000001)
PAM284: initial attenuator settings: 0dB(E), 0dB(N)
PAM284: trying attenuator setting 10dB: Success (E), Success (N)
PAM284: trying attenuator setting 0dB: Success (E), Success (N)
PAM284: supply sensor: 5.508V, 500.2mA
PAM284: RF power: -11.05dBm(E), -8.05dBm(N)
