
Date: 06 Nov 2020  12:51:06
Comment:  

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM292
Found: PAM292

FEM292: noise source data:
FEM292: 50.000MHz:29.89dB,70.000MHz:29.90dB,100.000MHz:30.03dB,150.000MHz:30.16dB,200.000MHz:30.16dB
FEM292: temp sensor type Si7021, Mk3 FEM
FEM292: sensor ID: 1470566488635473919 (0x1468806e15b5ffff)
FEM292: initial switch state (GPIO): 0b11111
FEM292: set switch (GPIO) state to load: 0b11000, Success
FEM292: set switch (GPIO) state to noise: 0b11001, Success
FEM292: set switch (GPIO) state to antenna: 0b11111, Success
FEM292: temperature: 35.52C
FEM292: humidity: 20.86%
FEM292: supply sensor: 7.304V, 467.8mA, OK
FEM292: position sensor: theta=92.197, phi=44.528
FEM292: barometer: pressure=1027.9hPa, altitude=-129.0m

PAM292: ROM ID: 8070590586853261413 (0x70007f6101000065)
PAM292: initial attenuator settings: 0dB(E), 0dB(N)
PAM292: trying attenuator setting 10dB: Success (E), Success (N)
PAM292: trying attenuator setting 0dB: Success (E), Success (N)
PAM292: supply sensor: 5.504V, 488.6mA
PAM292: RF power: -9.198dBm(E), -12.94dBm(N)
