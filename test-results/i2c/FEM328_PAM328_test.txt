
Date: 23 Sep 2021  17:07:07
Comment:  No fibre connected.

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM328
Found: PAM328

FEM328: noise source data:
FEM328: 50.000MHz:30.05dB,70.000MHz:30.09dB,100.000MHz:30.16dB,150.000MHz:30.32dB,200.000MHz:30.34dB
FEM328: temp sensor type Si7021, Mk3 FEM
FEM328: sensor ID: 17779664648605663231 (0xf6be0ecc15b5ffff)
FEM328: initial switch state (GPIO): 0b11110
FEM328: set switch (GPIO) state to load: 0b11000, Success
FEM328: set switch (GPIO) state to noise: 0b11001, Success
FEM328: set switch (GPIO) state to antenna: 0b11110, Success
FEM328: temperature: 29.34C
FEM328: humidity: 41.36%
FEM328: supply sensor: 7.268V, 469.0mA, OK
FEM328: position sensor: theta=92.5, phi=-56.97
FEM328: barometer: pressure=1015.5hPa, altitude=-19.7m

PAM328: ROM ID: 8107995972430200889 (0x7085636101000039)
PAM328: initial attenuator settings: 0dB(E), 0dB(N)
PAM328: trying attenuator setting 10dB: Success (E), Success (N)
PAM328: trying attenuator setting 0dB: Success (E), Success (N)
PAM328: supply sensor: 5.516V, 486.9mA
PAM328: RF power: -31.89dBm(E), -32.3dBm(N)
