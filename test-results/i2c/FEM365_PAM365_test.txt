
Date: 01 Feb 2022  20:30:29
Comment:  

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM365
Found: PAM365

FEM365: noise source data:
FEM365: 50.000MHz:29.68dB,70.000MHz:29.72dB,100.000MHz:29.79dB,150.000MHz:29.87dB,200.000MHz:29.86dB
FEM365: temp sensor type Si7021, Mk3 FEM
FEM365: sensor ID: 203211773769547775 (0x2d1f40815b5ffffL)
FEM365: initial switch state (GPIO): 0b11110
FEM365: set switch (GPIO) state to load: 0b11000, Success
FEM365: set switch (GPIO) state to noise: 0b11001, Success
FEM365: set switch (GPIO) state to antenna: 0b11110, Success
FEM365: temperature: 36.21C
FEM365: humidity: 20.37%
FEM365: supply sensor: 7.268V, 450.1mA, OK
FEM365: position sensor: theta=91.714, phi=-42.575
FEM365: barometer: pressure=1017.3hPa, altitude=-35.91m

PAM365: ROM ID: 8094808429966655683 (0x70568961010000c3)
PAM365: initial attenuator settings: 0dB(E), 0dB(N)
PAM365: trying attenuator setting 10dB: Success (E), Success (N)
PAM365: trying attenuator setting 0dB: Success (E), Success (N)
PAM365: supply sensor: 5.476V, 483.8mA
PAM365: RF power: -9.061dBm(E), -13.43dBm(N)
