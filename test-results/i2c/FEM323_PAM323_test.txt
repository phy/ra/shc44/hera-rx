
Date: 23 Sep 2021  16:50:04
Comment:  No fibre connected.

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM323
Found: PAM323

FEM323: noise source data:
FEM323: 50.000MHz:30.18dB,70.000MHz:30.18dB,100.000MHz:30.20dB,150.000MHz:30.26dB,200.000MHz:30.28dB
FEM323: temp sensor type Si7021, Mk3 FEM
FEM323: sensor ID: 4925743875835166719 (0x445bc1f515b5ffff)
FEM323: initial switch state (GPIO): 0b11110
FEM323: set switch (GPIO) state to load: 0b11000, Success
FEM323: set switch (GPIO) state to noise: 0b11001, Success
FEM323: set switch (GPIO) state to antenna: 0b11110, Success
FEM323: temperature: 31.4C
FEM323: humidity: 36.97%
FEM323: supply sensor: 7.276V, 455.5mA, OK
FEM323: position sensor: theta=93.696, phi=-52.739
FEM323: barometer: pressure=1015.6hPa, altitude=-20.75m

PAM323: ROM ID: 8096462095454830806 (0x705c6961010000d6)
PAM323: initial attenuator settings: 0dB(E), 0dB(N)
PAM323: trying attenuator setting 10dB: Success (E), Success (N)
PAM323: trying attenuator setting 0dB: Success (E), Success (N)
PAM323: supply sensor: 5.516V, 485.3mA
PAM323: RF power: -28.43dBm(E), -31.48dBm(N)
