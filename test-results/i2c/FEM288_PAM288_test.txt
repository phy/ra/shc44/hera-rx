
Date: 06 Nov 2020  11:41:51
Comment:  

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM288
Found: PAM288

FEM288: noise source data:
FEM288: 50.000MHz:30.44dB,70.000MHz:30.43dB,100.000MHz:30.40dB,150.000MHz:30.43dB,200.000MHz:30.34dB
FEM288: temp sensor type Si7021, Mk3 FEM
FEM288: sensor ID: 3676985905504452607 (0x3307473315b5ffff)
FEM288: initial switch state (GPIO): 0b11111
FEM288: set switch (GPIO) state to load: 0b11000, Success
FEM288: set switch (GPIO) state to noise: 0b11001, Success
FEM288: set switch (GPIO) state to antenna: 0b11111, Success
FEM288: temperature: 35.52C
FEM288: humidity: 21.83%
FEM288: supply sensor: 7.3V, 481.5mA, OK
FEM288: position sensor: theta=92.658, phi=46.696
FEM288: barometer: pressure=1029.0hPa, altitude=-138.1m

PAM288: ROM ID: 8085755051223548098 (0x70365f61010000c2)
PAM288: initial attenuator settings: 0dB(E), 0dB(N)
PAM288: trying attenuator setting 10dB: Success (E), Success (N)
PAM288: trying attenuator setting 0dB: Success (E), Success (N)
PAM288: supply sensor: 5.5V, 499.6mA
PAM288: RF power: -14.74dBm(E), -7.013dBm(N)
