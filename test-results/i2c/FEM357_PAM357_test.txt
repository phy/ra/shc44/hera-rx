
Date: 13 Jan 2022  20:20:19
Comment:  

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM357
Found: PAM357

FEM357: noise source data:
FEM357: 50.000MHz:30.05dB,70.000MHz:30.08dB,100.000MHz:30.16dB,150.000MHz:30.24dB,200.000MHz:30.27dB
FEM357: temp sensor type Si7021, Mk3 FEM
FEM357: sensor ID: 5875325157449793535 (0x5189591a15b5ffff)
FEM357: initial switch state (GPIO): 0b11110
FEM357: set switch (GPIO) state to load: 0b11000, Success
FEM357: set switch (GPIO) state to noise: 0b11001, Success
FEM357: set switch (GPIO) state to antenna: 0b11110, Success
FEM357: temperature: 32.77C
FEM357: humidity: 19.39%
FEM357: supply sensor: 7.264V, 453.1mA, OK
FEM357: position sensor: theta=92.944, phi=-43.09
FEM357: barometer: pressure=1037.3hPa, altitude=-208.1m

PAM357: ROM ID: 8073388843945951429 (0x700a7061010000c5)
PAM357: initial attenuator settings: 0dB(E), 0dB(N)
PAM357: trying attenuator setting 10dB: Success (E), Success (N)
PAM357: trying attenuator setting 0dB: Success (E), Success (N)
PAM357: supply sensor: 5.492V, 489.7mA
PAM357: RF power: -7.668dBm(E), -6.384dBm(N)
