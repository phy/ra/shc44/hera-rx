
Date: 28 Sep 2021  16:03:30
Comment: No fibre connected 

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM341
Found: PAM341

FEM341: noise source data:
FEM341: 50.000MHz:30.26dB,70.000MHz:30.37dB,100.000MHz:30.41dB,150.000MHz:30.46dB,200.000MHz:30.33dB
FEM341: temp sensor type Si7021, Mk3 FEM
FEM341: sensor ID: 1111080839811563519 (0xf6b5a2315b5ffffL)
FEM341: initial switch state (GPIO): 0b11110
FEM341: set switch (GPIO) state to load: 0b11000, Success
FEM341: set switch (GPIO) state to noise: 0b11001, Success
FEM341: set switch (GPIO) state to antenna: 0b11110, Success
FEM341: temperature: 26.6C
FEM341: humidity: 42.83%
FEM341: supply sensor: 7.264V, 470.3mA, OK
FEM341: position sensor: theta=91.512, phi=-49.912
FEM341: barometer: pressure=1011.2hPa, altitude=17.29m

PAM341: ROM ID: 8135307841264156829 (0x70e66b610100009d)
PAM341: initial attenuator settings: 0dB(E), 0dB(N)
PAM341: trying attenuator setting 10dB: Success (E), Success (N)
PAM341: trying attenuator setting 0dB: Success (E), Success (N)
PAM341: supply sensor: 5.52V, 486.9mA
PAM341: RF power: -30.97dBm(E), -32.09dBm(N)
