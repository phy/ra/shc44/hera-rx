
Date: 28 Sep 2021  16:00:48
Comment: No fibre connected 

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM339
Found: PAM339

FEM339: noise source data:
FEM339: 50.000MHz:29.80dB,70.000MHz:29.85dB,100.000MHz:29.96dB,150.000MHz:30.14dB,200.000MHz:30.20dB
FEM339: temp sensor type Si7021, Mk3 FEM
FEM339: sensor ID: 12611670762075848703 (0xaf05a7bd15b5ffff)
FEM339: initial switch state (GPIO): 0b11110
FEM339: set switch (GPIO) state to load: 0b11000, Success
FEM339: set switch (GPIO) state to noise: 0b11001, Success
FEM339: set switch (GPIO) state to antenna: 0b11110, Success
FEM339: temperature: 25.22C
FEM339: humidity: 45.27%
FEM339: supply sensor: 7.28V, 447.7mA, OK
FEM339: position sensor: theta=93.049, phi=-49.087
FEM339: barometer: pressure=1011.3hPa, altitude=17.01m

PAM339: ROM ID: 8118136768173178881 (0x70a96a6101000001)
PAM339: initial attenuator settings: 0dB(E), 0dB(N)
PAM339: trying attenuator setting 10dB: Success (E), Success (N)
PAM339: trying attenuator setting 0dB: Success (E), Success (N)
PAM339: supply sensor: 5.524V, 486.5mA
PAM339: RF power: -31.62dBm(E), -31.87dBm(N)
