
Date: 22 Dec 2021  16:27:10
Comment:  

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: 20 21 -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- 36 -- -- -- -- -- -- -- -- -- 
40: 40 -- -- -- 44 45 -- -- -- -- -- -- -- -- -- -- 
50: 50 51 52 -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: -- -- -- -- -- -- -- -- -- 69 -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- 77                         

Found: FEM353
Found: PAM353

FEM353: noise source data:
FEM353: 50.000MHz:29.91dB,70.000MHz:29.94dB,100.000MHz:30.01dB,150.000MHz:30.10dB,200.000MHz:30.11dB
FEM353: temp sensor type Si7021, Mk3 FEM
FEM353: sensor ID: 1923409801463070719 (0x1ab1530615b5ffff)
FEM353: initial switch state (GPIO): 0b11110
FEM353: set switch (GPIO) state to load: 0b11000, Success
FEM353: set switch (GPIO) state to noise: 0b11001, Success
FEM353: set switch (GPIO) state to antenna: 0b11110, Success
FEM353: temperature: 33.46C
FEM353: humidity: 18.9%
FEM353: supply sensor: 7.264V, 447.7mA, OK
FEM353: position sensor: theta=92.356, phi=-44.678
FEM353: barometer: pressure=1011.6hPa, altitude=14.38m

PAM353: ROM ID: 8118983392126566518 (0x70ac6c6101000076)
PAM353: initial attenuator settings: 0dB(E), 0dB(N)
PAM353: trying attenuator setting 10dB: Success (E), Success (N)
PAM353: trying attenuator setting 0dB: Success (E), Success (N)
PAM353: supply sensor: 5.52V, 487.1mA
PAM353: RF power: -12.67dBm(E), -11.85dBm(N)
