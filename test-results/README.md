## Test results

### I<sup>2</sup>C

Test results include test/fail indications, FEM version, supply currents etc.

### RF

Three port VNA measurement files in Touchstone format for the RF chain; FEM, 500m fibre, PAM.