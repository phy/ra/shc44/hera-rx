## CAN bus adapter

This is an adapter for testing HERA FEMs/PAMs in the field or lab. It converts I<sup>2</sup>C to/from the psuedo CAN bus physical layer. This is an implementation of I<sup>2</sup>C over two differential pairs using CAN bus transceivers to support long distances (500m).

The adapter can be terminated (100Ω) with jumpers as a CAN bus end point, or left high impedance when used as a mid-bus node. Note CAN bus impedance is nominally 120Ω but the HERA implementation is matched for Cat 7 cable impedance (100Ω).

There are two grounds; 'Earth', the Cat 7 cable screen and 'GND', the signal or local ground. There is a resistor between the two to dump electrostatic charge. Having the two grounds available on a header is useful for measuring noise or potential differences between the grounds in the field.

If there is no Ethernet cable screen, the GND-Earth jumper makes no difference. If unsure, or in the lab, leave the ground pins connected with a jumper.

**For lab use any CAT 4-6 Ethernet cable, screened or unscreened, can be used.**

The adapter is powered by the host; either a Bus Pirate, or Raspberry Pi. The Bus Pirate option is best for field use since this only requires a serial console over USB.

![CAN bus adapter](images/CANbus_adapter.png)

### Device addresses

| Device | IC | 7-bit address (normal) | 8-bit address (Bus Pirate write) |
|--------|----|------------------------|----------------------------------|
| Magnetometer | MPU-9250 | 0x0C | 0x18 |
| GPIO for noise switch on FEM | PCF8574TS/3 | 0x20 | 0x40 |
| GPIO for attenuator on PAM | PCF8574TS/3 | 0x21 | 0x42 |
| FEM Temperature sensor | Si7021 | 0x40 | 0x80 |
| PAM voltage IC for current sensing | INA219 | 0x44 | 0x88 |
| FEM voltage IC for current sensing | INA219 | 0x45 | 0x8A |
| PAM voltage IC for power level detector | MAX11644 | 0x36 | 0x6C |
| PAM Serial number | DS28CM00 | 0x50 | 0xA0 |
| EEPROM on FEM | 24LC64 | 0x51 | 0xA2 |
| EEPROM on PAM | 24LC64 | 0x52 | 0xA4 |
| IMU on FEM | MPU-9250 | 0x69 | 0xD2 |
| Barometer on FEM | MS5611 | 0x77 | 0xEE |

For the Bus Pirate read address, add 1 to the 8-bit address.

The EEPROMs are 64k bit, the PAM serial number is a 64 bit factory programmed ROM.

### Raspberry Pi host

This is suited for lab use. The six pin header mates with the lowest six pins of the 40 pin Pi GPIO header. Use a 40 way ribbon cable, cutting short the upper 34 wires, terminate the adapter end in a six way header.

![Pi method](images/pi_option.jpg)

If using the *SMBus* (System Management Bus) library the Pi I<sup>2</sup>C clock should be set to 10kHz by appending this to */boot/config.txt*:

    dtparam=i2c_baudrate=10000

Ensure *SMBus* is available:

    sudo apt-get install python-smbus python3-smbus python-dev python3-dev

These steps are not required with software I2C using *pigpio* and *casperfpga* libraries since baudrate can be set on-the-fly with pigpio.

The Pi hardware I<sup>2</sup>C does not have its own pullups, these are provided on the adapter. The 10kΩ resistors are sufficient for low capacitance loads, e.g. a 20cm header cable between the Pi and CAN bus adapter.

The example Python scripts, tested with Python 2.7 and 3.7, are used to program the PAM or FEM serial numbers. Use argument zero to read the first 32 bytes of memory space (read only), e.g.:

    $ python pam_write.py 221

    Writing serial number...
    PAM221 ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

    $ python pam_write.py 0

    Read only:
    PAM221 ▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪▪

### Bus Pirate host

"The Bus Pirate is an open source hacker multi-tool that talks to electronic stuff" [(Dangerous Prototypes)](http://dangerousprototypes.com/docs/Bus_Pirate).

This is suited for field use since it can be powered by USB from a laptop. It has been successfully used with an Android tablet running [DroidScript](https://github.com/JamesNewton/DroidScriptBusPirate) and an OTG USB adapter.

This Bus Pirate is hardware version 3.6a manufactured and sold by [Sparkfun](https://www.sparkfun.com/products/12942). There are only cosmetic differences between hardware versions 3.5 and 3.6a. The recommended community firmware is _v6.3-beta1 r2151_. There is also custom firmware for download which enables I<sup>2</sup>C at 10kHz, see the firmware folder. There is no issue using the Bus Pirate at 5kHz baudrate.

The Bus Pirate has its own pullup resistors for I<sup>2</sup>C.

![CAN bus adapter](images/header_orientation.jpg)

**Some SparkFun boards have reversed headers, causing some confusion. Confirm that adapter ground goes to BP ground before applying power.**

Bus Pirate hardware version 4 has a different (12 pin) header pinout and is not compatible with this adapter.

Bus Pirate uses I<sup>2</sup>C addresses in 8 bit format, where the LSB is the R/W bit (see table).

#### Initialisation

Connect a serial console to the Bus Pirate at 115200 baud and Send the following (_?_ for a list of commands):

| Command | Description         |
|---------|---------------------|
| m4      | set mode to I2C     |
| 1       | set speed ~5kHz     |
| W       | power supply on     |
| P       | pullup resistors on |

The HERA CAN bus clock operates at 10kHz and the closest Bus Pirate speed is 5kHz This works without any issue.

#### I<sup>2</sup>C read

**Example:** the FEM EEPROM is a 24LC64 type at address 0xA2. The first 105 bytes contain the FEM serial number, the noise source data and a null character. E.g:

`FEM101 NS={50.000MHz:30.34dB,70.000MHz:30.08dB,100.000MHz:30.06dB,150.000MHz:29.99dB,200.000MHz:29.92dB}`

The last string in EEPROM must be terminated by a null character (0x00). To read the serial number send:

    [0xa2 0 0 [0xa3 r:6]

Typical response is:

    READ: 0x46  ACK 0x45  ACK 0x4D  ACK 0x30  ACK 0x31  ACK 0x34

The ASCII decodes to _FEM014_

To set the Bus Pirate to decode ASCII, send:

| Command | Description         |
|---------|---------------------|
| o       | set output type     |
| 4       | raw (decode ASCII)  |

Then repeat the previous command string. The typical response is:

    READ: F  ACK E  ACK M  ACK 0  ACK 1  ACK 4

#### I<sup>2</sup>C write

To write a single byte, e.g. 0x30 to address 0x0003:

    [0xa2 0 0x03 0x30]

Up to one page (32 bytes) can be written in one line providing the write doesn't cross a page boundary. To program serial number _FEM014_ in one line:

    [0xA2 0 0 0x46 0x45 0x4D 0x30 0x31 0x34]

#### Other commands

| Command | Description             |
|---------|-------------------------|
| w (lower case) | power supply off |
| ?              | help             |
| #              | reset & power off|

### Acknowledgements

Thanks Lindsay and Danny at ASU, and Lynne at PCBway for finding the PCB mask error. The missing solder masks on the RJ45 connectors have been corrected.

Thanks Tian for the superb *Digital Control* documentation.

### Further information

- Huang T. et al, 2018. *HERA RX Digital Control*. Department of Physics, University of Cambridge.
- Microchip, 2012. *24LC64 64K Serial EEPROM Datasheet*. Microchip Technology Inc.
- HERA Python control scripts.
