## Firmware

Custom Bus Pirate firmware to enable I<sup>2</sup>C at 10kHz. Based on [Community firmware](https://github.com/BusPirate/Bus_Pirate) version 7.1.

How to upgrade: [Pirate-Loader console upgrade application](http://dangerousprototypes.com/docs/Pirate-Loader_console_upgrade_application_%28Linux,_Mac,_Windows%29)

The following error can be safely ignored:

    Erasing page 42, a800...ERROR [50]
    Error updating firmware :(

This is because of bootloader protection, [see here](http://dangerousprototypes.com/forum/index.php?topic=447.0#p3873).
