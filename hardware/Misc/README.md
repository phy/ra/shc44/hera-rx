## Miscellaneous tools / graphics


### Node signs

![Node signs](node_signs.jpg)

Node signs made by Ziyaad in MakeSpace, Cambridge. Text height 38mm. _'Plain'_ versions have the fonts converted to paths (these view correctly without special fonts installed). The stencil font is _'Gunplay'_.


### Feed targets

![Feed alignment target](HERA_target.png)

Feed alignment target; concentric circles (120mm max), self-adhesive vinyl cut by CNC vinyl cutter. Rings can have different colours.


### HERA sleeve tool

Use with 25.4mm diameter heatshrink tube (RS 399-647), cut to 34mm lengths.

Files optimised for laser cutter with 0.15mm kerf. Cut from 3mm plywood.


### FEM alignment check tool

![Alignment check tool](check_disk.png)

Cut from 3mm clear acrylic (Perspex). Countersink inner four holes to reduce the risk of catching on feed pins ridges.
