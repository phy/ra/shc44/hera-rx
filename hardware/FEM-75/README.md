## FEM-75

![FEM-75 Assembly](hera_fem75_assembly.PNG)

Enclosure for the HERA 75Ω Front End Module. Attaches to the PAPER dipole using an adapter ring: "balun_flange_adapter".

The 3D models were designed with SolidWorks 2016. These files import into Autodesk Inventor 2018 without issue. Finish for the FEM aluminium parts is Alocom 1200 (UK) or Alodine 1200 (US) and gloss white powder coat or paint for the body external surfaces.

PCB source files are Altium release 14.
