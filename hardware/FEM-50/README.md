## FEM-50

![FEM-50 Assembly](FEM-50_assembly3.PNG)

Enclosure for the [HERA](http://reionization.org/) 50Ω Front End Module (2018) for the Vivaldi antenna feeds. Can also work with [PAPER](http://eor.berkeley.edu/) antennas with an alternative transition board and the following adapter parts: ["fem_adapter_ring"](fem_adapter_ring.pdf), ["fem_flange-5mm"](fem_flange-5mm.pdf).

The body is a 200mm long extruded aluminium tube case from Fischer Elektronik GmbH (part number [UTG-7166-200-TP](https://www.fischerelektronik.de/web_fischer/en_GB/$catalogue/fischerData/PR/UTG7166_/datasheet.xhtml)). The transparent passivated finish is electrically conductive. The flange gasket is 1.5mm nickel graphite loaded elastomer.

This 3D model was created with SolidWorks 2016 [(eDrawing version here)](FEM-50_assembly3.EASM), it imports into Inventor 2018 without errors, or it can be viewed with the [free eDrawings viewer](http://www.edrawingsviewer.com/) Finish for the 100mm diameter flange is Alocom 1200 (UK) or Alodine 1200 (US) and white gloss powder coat for the extrusion external surfaces.

The PCB files are Altium release 14.


**Using PAPER antenna feeds**

Parts _fem_adapter_ring_ and _fem_flange-5mm_ are only required to adapt the FEM-50 to the old [PAPER](http://eor.berkeley.edu/) antenna feeds.
