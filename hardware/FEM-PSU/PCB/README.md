### FEM-PSU PCB

![PCB](PCB_full_length.png)

**Schematic**
* [FEM_power_supply.pdf](FEM_power_supply.pdf)

**Note:**
* The eight LDO regulators (U1) must be aligned by their tabs on a fixture before soldering to the PCB. The fixture is made from 1.6mm FR4 with Gerber data: "LDO_reg_alignment_tool".
* Two boards are used in each power supply; full length and a 'short' version made by cutting the full board on the marked 'cut line'.

PCB source files are Altium release 16.

![Regulator alignment tool](fem_psu_board_fixture.jpg)

*Tool to align the voltage regulators with the heat spreader bar.*

Sometimes we have a edge components that clearly have to mount through another panel but finished assemblies are returned with these components pointing in different directions. This tool, made from 1.6mm FR4, will ensure the regulators line up with the heatsink spreader bar. The tool must be flush with and 90° to the board.

**Versions:**
* 18/1/2018 - first external fabrication
* 31/5/2018 - KK396 connector pitch increased from 150 to 156mils (3.96mm)
* 30/8/2018 - Legend for U2 (78L05) now has correct orientation

The LEDs require custom FDM printed ABS mount from the file: "LED_mount.SLDPRT", the .stl file included for direct 3D printing.
![LED mount](LED_altium.png)
*FDM printed mount + LED*
