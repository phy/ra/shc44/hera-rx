## FEM-PSU

![FEM Power Supply](FEM_PSU.PNG)

**Low noise linear power supply for the [HERA](http://reionization.org/) Front End Modules.**

* 12 x 7.5V outputs
* can be modified for 115Vac input
* independent 800mA current limits
* foldback current limiting
* EMC screening kit fitted to case
* bi-colour LED: red (supply on), green (load on).

**Schematic** [in the PCB folder](PCB/FEM_power_supply.pdf)

**Assembly**

* [Complete 3D model](FEM_PSU.EASM) (SolidWorks 2016)
* SolidWorks parts (SLDPRT) import into Inventor 2018 without issue
* 3D STEP models available (AP214 version).

**Maintenance**

Recommended post shipping check; remove top cover, check for loose screws and components. Note after serial *FPS05* all threads (except self-tapping and SMA jacks) have thread locking compound.

Safety (UK); operating, for maintenance, on low voltage sections with the top cover removed is permitted providing the clear polycarbonate shield is securely installed.

**Tools**

* Torx T8 for top & bottom panels
* Tork T20 for front & rear panels
* 8mm box spanner to spin off SMA nuts
* 7mm flat spanner for bridge rectifier spacers
* 2.5mm ball-end hex key for regulator to heat sink screws


----
![Internal view](fem_psu_open.jpg)

*Internal view*
