## Gallery

Radio-astronomy instruments don't die, they fade away, or get re-purposed (Greta would be proud). 

!["re-purposed PAPER compatible front-end module"](fem_75_a.jpg)
HERA front-end module, PAPER feed version.

!["re-purposed HERA front-end module"](fem_50_a.jpg)
HERA front-end module, rejected but not discarded.
