## RF Hardware

### FEM-75

Enclosure for the HERA 75Ω Front End Module (obsolete 2018). Attaches to the PAPER dipole using an adapter ring: ["balun_flange_adapter"](FEM-75/FEM75_flange_adapter.PDF).

### FEM-50

[HERA](http://reionization.org/) 50Ω Front End Module (current) for the new Vivaldi antenna feeds referred to as simply FEM (from 2018).

Can also work with [PAPER](http://eor.berkeley.edu/) antennas using an alternative transition board and the following adapters: ["fem_adapter_ring"](FEM-50/fem_adapter_ring.pdf),  ["fem_flange-5mm"](FEM-50/fem_flange-5mm.pdf).

The body is a 200mm long COTS aluminium extrusion from Fischer Elektronik GmbH, part number [UTG-7166-200-TP](https://www.fischerelektronik.de/web_fischer/en_GB/$catalogue/fischerData/PR/UTG7166_/datasheet.xhtml). The *transparent passivated* finish is electrically conductive and a good primer for paint or powder coating. External surfaces are powder coated gloss white to reduce solar heating.

### FEM-PSU

Low noise linear power supply for the FEM-50 modules. 12 x 7.5V outputs (individually limited to 800mA).

### Miscellaneous

Alignment tools, signs, alignment targets etc

### Gallery

Re-cycle where possible.
