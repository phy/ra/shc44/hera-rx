#!/usr/bin/python

# Write serial number and noise data to FEM EEPROMs.
# Provide a 1-3 digit serial number as argument,
# argument 0 reads the serial space (read only).
#
# Requires noise data in a separate dictional file:
#   noise_data.py
#
# Reads back the first 108 bytes (data should be
# 104 bytes plus a null character (0x00).
#
# Tested on Python 2.7 and 3.7
# If unable to load smbus, install:
# sudo apt install python-smbus python3-smbus
#
# EEPROM (24LC64) I2C addresses:
#   FEM: 0x51
#
# Author: Steve Carey
#         Cavendish Lab
# Date:   7 August 2020

from noise_data import ns_data
import sys
import smbus
import time

bus = smbus.SMBus(1)

if len(sys.argv) < 2:
    sys.stderr.write("usage: python %s <serial_number> (0-999), 0 for read only.\n" % (sys.argv[0],))
    raise SystemExit

serial_num = int(sys.argv[1])  # serial number (0-999)
if serial_num < 0 or serial_num > 999:
    print("Serial number out of range (0-999)")
    raise SystemExit

dev_addr = 0x51                # FEM EEPROM I2C address
mem_addr = 0x0000              # memory start address (16 bit)

if serial_num != 0:
    # Write FEM serial number and noise with data from dictionary file
    data = ns_data.get(serial_num)  # get data string from dictionary file
    if data == None:
        print ("Serial number not found in noise_source_data.py")
        raise SystemExit
    
    def write(dev_addr, mem_addr, data):
        mem_hi = mem_addr >> 8                            # address high byte	
        bytes = [mem_addr & 0xff] + list(map(ord, data))  # first data byte is address low byte
        print("writing to %.2x at %.4x bytes %d" % (dev_addr, mem_addr, len(data)))
        return bus.write_i2c_block_data(dev_addr, mem_hi, bytes)

    page = []
    i = 0
    while i * 16 < len(data):  # use 16 byte pages, smbus limitation
        page = data[i * 16:(i + 1) * 16]
        write(dev_addr, i * 16, page)
        time.sleep(0.1)        # allow time to complete block write or there be errors
        i += 1
    time.sleep(0.1)

    # Terminate data by writing a null (0x00)
    append_addr = mem_addr + len(data)  # write null (0x00) after data
    append_hi = append_addr >> 8        # address high byte
    print("writing null to %.2x at %.4x (%d)" % (dev_addr, append_addr, append_addr))
    bus.write_i2c_block_data(dev_addr, append_hi, [append_addr & 0xff, 0x00])
    time.sleep(0.1)
    bus.read_byte(dev_addr)             # release I2C after write
    
    print("Data from file:")
    print(data)
    print("Readback...")
else:
    print("Read only:")


size = 114                     # max EEPROM string length, normal length 104 chars
#There has been a reliability issue (writing single bytes).
#Swapping single byte writes for block writes fixed the issue:
#bus.write_byte(dev_addr, mem_addr >> 8)
#bus.write_byte(dev_addr, mem_addr & 0xff)
bus.write_i2c_block_data(dev_addr, mem_addr >> 8, [mem_addr & 0xff])
for i in range(0,size):
    byte = bus.read_byte(dev_addr)
    if byte == 0x00:                    # quit if null string terminator found
        break
    sys.stdout.write(chr(byte))
print("")
time.sleep(0.1)
bus.read_byte(dev_addr)                 # release I2C after write
