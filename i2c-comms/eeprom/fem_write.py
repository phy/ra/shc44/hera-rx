#!/usr/bin/python

# Write serial number to PAM/FEM EEPROMs.
# Provide a 1-3 digit serial number as argument,
# argument 0 reads the serial space (read only).
#
# Always reads back the first 32 bytes to show if
# other stuff is present, e.g. noise source data.
#
# Tested on Python 2.7.16 and 3.7.3
# If unable to load smbus, install:
# sudo apt install python-smbus python3-smbus
#
# EEPROM (24LC64) I2C addresses:
#   PAM: 0x52
#   FEM: 0x51
#
# Author: Steve Carey
#         Cavendish Lab
# Date:   7 August 2020

import sys
import smbus
import time

type = "FEM"                # define PAM or FEM

bus = smbus.SMBus(1)

if len(sys.argv) < 2:
    sys.stderr.write("usage: python %s <serial_number> (0-999), 0 for read only, 999 to erase data\n" % (sys.argv[0],))
    raise SystemExit

if type == "PAM":
    dev_addr = 0x52         # PAM EEPROM I2C address 
elif type == "FEM":
    dev_addr = 0x51         # FEM EEPROM I2C address
else:
    sys.exit(1)             # Future module options?
mem_addr = 0x0000           # memory start address (16 bit)

mem_hi = mem_addr >> 8      # 16 bit memory address MS byte
mem_lo = mem_addr & 0xff    # 16 bit memory address LS byte

sn = int(sys.argv[1])       # serial number (0-999)
if sn < 0 or sn > 999:
    print("Serial number out of range")
    sys.exit(1)

sn2 = sn / 100 + 0x30       # MS serial byte - NB all integer calculations 
sn1 = sn % 100 / 10 + 0x30  # mid serial byte
sn0 = sn % 10 + 0x30        # LS serial byte

# Write to the EEPROM
if sn != 0:                 # if serial number is 0, only read the memory space
    print("Writing serial number...")
    if type == "PAM":
        if sn == 999:                # erase by writing xxx for the serial number
            bus.write_i2c_block_data(dev_addr, mem_hi, [mem_lo, 0x50, 0x41, 0x4d, 0x78, 0x78, 0x78, 0x20, 0x00])
            time.sleep(0.1)          # allow time to complete block write or there be errors
            bus.read_byte(dev_addr)  # release I2C after write
        else:
            bus.write_i2c_block_data(dev_addr, mem_hi, [mem_lo, 0x50, 0x41, 0x4d, sn2, sn1, sn0, 0x20, 0x00])
            time.sleep(0.1)          # allow time to complete block write or there be errors
            bus.read_byte(dev_addr)  # release I2C after write
    elif type == "FEM":
        if sn == 999:
            bus.write_i2c_block_data(dev_addr, mem_hi, [mem_lo, 0x46, 0x45, 0x4d, 0x78, 0x78, 0x78, 0x20])
            time.sleep(0.1)
            bus.read_byte(dev_addr)
        else:
            bus.write_i2c_block_data(dev_addr, mem_hi, [mem_lo, 0x46, 0x45, 0x4d, sn2, sn1, sn0, 0x20])
            time.sleep(0.1)
            bus.read_byte(dev_addr)
else:
    print("Read only:")
    

# Read back the EEPROM
bus.write_i2c_block_data(dev_addr, mem_hi, [mem_lo])
for i in range(0, 6):
    byte = bus.read_byte(dev_addr)
    sys.stdout.write(chr(byte))
print("")    # quotes for compatibility between Python versions
