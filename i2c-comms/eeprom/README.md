## EEPROM

Python 3 scripts to configure FEM/PAM modules by writing to the 64k bit EEPROMs. This folder also contains the noise source data for each FEM in a Python dictionary: [*noise_data.py*](noise_data.py)

- *nsdata_write.py* - writes FEM serial number and noise data,  
requires *noise_data.py* in the same directory.
- *fem_write.py* - write FEM serial number,  
this partly duplicates *nsdata_write.py* which also writes the FEM serial number.
- *pam_write.py* - write PAM serial number.
- *data_entry.py* - helps build the noise source dictionary file.

Noise source data must be present in the Python dictionary: *noise_data.py*  


### Setup
#### Hardware

![Setup diagram](img/CANbus_test_setup.png)

*Fig 1.* Setup in Cambridge for production testing.

![Setup diagram (alternate)](img/CANBus_test_setup_alternate.png)

*Fig 2.* Setup without cable adapter.

![Setup diagram (alternate)](img/CANBus_test_setup_alternate_karoo.png)

*Fig 3.* Setup for testing on site at the Karoo KAPB (screened room).

![Canbus adapter and cable adapter](img/test_adapters.jpg)
*Fig 4.* Canbus and cable adapters.

The cable adapter 7V connection is not required when the adapter is connected to the PAM rack (via the 37 way D-connector) where the PAMs obtain their power from the rack PSU.

#### Software
Install SMBus:

    sudo apt update
    sudo apt-get install python-smbus python3-smbus python-dev python3-dev i2c-tools

The scripts in this directory access I<sup>2</sup>C through the System Management Bus *(SMBus)* library. This requires the clock set to 10kHz in the Pi *config.txt* file. The SMBus derives the I<sup>2</sup>C clock from the Pi VPU core which can vary with the Pi 3 and 4 due to thermal regulation. if the I<sup>2</sup>C clock deviates significantly from 10kHz, the VPU clock can be fixed. These setting are appended in */boot/config.txt*:

    dtparam=i2c_baudrate=10000
    core_freq=250    # fix VPU core clock for the Pi 3&4 (not required for Pi 2)

The *GND-Earth* jumper on the CAN bus adapter is for measuring ground loop currents in the field. This jumper should otherwise be left closed.

### Usage

python fem_write | pam_write | nsdata_write &#60;serial_number (1-3 digits)&#62;

Note:

Entering a serial number of zero will read only and not write to the module.

Entering 999 will erase the serial number (set FEMxxx or PAMxxx)

The FEM serial number script only writes to the first 7 bytes (6 characters plus space). It will not overwrite noise source data.

Examples:

*python fem_write 0*  
Reads first 32 bytes of EEPROM from FEM and displays on screen. This can also show if noise data present.

*python pam_write 221*  
Writes 'FEM221 ' to the first seven bytes, including a trailing space, of EPPROM followed by readback of first 32 bytes.

*python data_write 0*  
Reads FEM EEPROM up to the first null (0x00) or maximum 114 bytes. Normal data length is 104 bytes plus the null (0x00).

*python data_write 221*  
Writes s/n and noise data to FEM, typically 104 bytes plus null. Data must be available in the Python dictionary file *noise_source_data.py* which must be in the same directory. Data from the file is printed to screen followed by a readback of data stored to the EEPROM.
