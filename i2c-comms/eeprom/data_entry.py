#!/usr/bin/python

# Facilitate entering FEM noise source into the
# dictionary file: noise_data.py
# Requires noise data in a separate dictional file:
#   noise_data.py
#
# Usage:
# This scripts appends dictionary pairs to 'new_data.txt'
# manually copy and paste lines from this file into 'noise_data.py'
#
# Author: Steve Carey
#         Cavendish Lab
# Date:   22 Sept 2021

from noise_data import ns_data
import sys

if len(sys.argv) < 2:
    sys.stderr.write("usage: python %s <starting serial_number> (0-999).\n" % (sys.argv[0],))
    raise SystemExit()

serial_num = int(sys.argv[1])  # serial number (0-999)
if serial_num == 0:
    print("S/N 0: exit")
    raise SystemExit()
if serial_num < 0 or serial_num > 999:
    print("Serial number out of range (0-999)")
    raise SystemExit()

while serial_num != 0:
    data = ns_data.get(serial_num)  # if serial not exist, start new entry
    if data != None:
        print("Serial number already exists")
        raise SystemExit()
    else:
        # New dictionary data entry
        print ("\nSerial number:", serial_num)
        print("(0 to exit)")
        f50 = float(input(" 50 MHz: "))
        if f50 == 0:
            print("Exit")
            raise SystemExit()
        f70 = float(input(" 70 MHz: "))
        f100 = float(input("100 MHz: "))
        f150 = float(input("150 MHz: "))
        f200 = float(input("200 MHz: "))
        
        # format dictionary value
        if serial_num < 100:
            newPair = "    "+str(serial_num)+": \"FEM0"+str(serial_num)+ \
                " NS={50.000MHz:"+str('%.2f' % f50)+"dB,70.000MHz:"+ \
                str('%.2f' % f70)+"dB,100.000MHz:"+str('%.2f' % f100)+ \
                "dB,150.000MHz:"+str('%.2f' % f150)+"dB,200.000MHz:"+ \
                str('%.2f' % f200)+"dB}\",\n"
        else:
            newPair = "    "+str(serial_num)+": \"FEM"+str(serial_num)+ \
                " NS={50.000MHz:"+str('%.2f' % f50)+"dB,70.000MHz:"+ \
                str('%.2f' % f70)+"dB,100.000MHz:"+str('%.2f' % f100)+ \
                "dB,150.000MHz:"+str('%.2f' % f150)+"dB,200.000MHz:"+ \
                str('%.2f' % f200)+"dB}\",\n"
        print(newPair)
        
        file = open("newData.txt", "a")
        file.write(str(newPair))
        file.close()
        
        serial_num += 1
        
        