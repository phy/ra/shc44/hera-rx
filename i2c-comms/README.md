## HERA I<sup>2</sup>C Communication

Programe and test I<sup>2</sup>C devices in the FEM and PAM modules.

### EEPROM
Scripts to read/write the 64k bit EEPROM in FEM/PAM modules. These hold serial numbers and FEM noise source calibration data. They require Python 3 and use Python-SMBus for I<sup>2</sup>C communication. The noise data is pulled from the Python dictionary: [*noise_data.py*](eeprom/noise_data.py)


### Test scripts

Python scripts for commissioning tests. These use *[pigpio](http://abyz.me.uk/rpi/pigpio/)* for I<sup>2</sup>C communication and modules originally written in 2018 for a minimal Python 2 running on FPGAs. Porting them to Python 3 has not yet been successful.

Since Python 2.7 was deprecated in 2020, installing the necessary dependancies is very time consuming. A  Raspberry Pi image (755MB) is available on request. Contact the author for this support.

## Configuration

![CAN bus diagram](topography.png)

*Fig 1.* Configuration on site.

Command communication is by I<sup>2</sup>C over CAN bus using Cat 7 cable; one pair for SCL, one pair for SDA. This permits a much greater length than the I<sup>2</sup>C specification allows. The PAM modules connect to the bus via short unterminated spurs.

For test configurations see the [EEPROM](eeprom/) folder.

### Sensor locations

![Sensor locations](FEM50_board.png)

### Device I<sup>2</sup>C addresses

| Address | Description | IC part | Format |
|---------|-------------|---------|--------|
| 0x0C | FEM magnetometer (disabled by default)| MPU-9250 | not used
| 0x20 | FEM GPIO antenna switch & RF amp enable | PCF8574TS/3 | see table 2 |
| 0x21 | PAM GPIO attenuator setting | PCF8574TS/3 | see table 2 |
| 0x36 | PAM voltage IC for power level detector | MAX11644 |
| 0x40 | FEM temperature sensor & ESN ROM (mk3) | Si7021 | ESN: 0xnnnnnnnn15nnnnnn |
| 0x40 | FEM temperature sensor & ESN ROM (mk1-2) | Si7051 | ESN: 0xnnnnnnnn33nnnnnn |
| 0x44 | PAM voltage IC for current sensing | INA219 |
| 0x45 | FEM voltage IC for current sensing (mk2-3) | INA219 |
| 0x4f | FEM voltage IC for current sensing (mk1) | LTC2990 |
| 0x50 | PAM Electronic Serial Number (ESN) ROM| DS28CM00 | 0x70nnnnnnnnnnnncc (cc=CRC)
| 0x51 | EEPROM on FEM | 24LC64 | "FEMnnn" (nnn="001"-"999")|
| 0x52 | EEPROM on PAM | 24LC64 | "PAMnnn" (nnn="001"-"999")|
| 0x69 | IMU on FEM | MPU-9250 |
| 0x77 | Barometer on FEM | MS5611 |

*Table 1 - I<sup>2</sup>C addresses*

The FEM revision (mk1/2 or mk3) can be determined from the ESN. Mk1 FEMs were only installed in node 0.

| GPIO bit | FEM | PAM |
|----------|-----|-----|
| 0b11111111 | power-on default | power-on default |
| 0bxxxxx000 | load |
| 0bxxxxx001 | noise |
| 0bxxxxx11x | antenna \* |
| 0bxxxx1xxx | RF amp enable E (mk3 \*\*) |
| 0bxxx1xxxx | RF amp enable N (mk3 \*\*) |
| 0bxxxxn&#772;n&#772;n&#772;n&#772; |  | E attenuation n dB \*\*\* |
| 0bn&#772;n&#772;n&#772;n&#772;xxxx |  | N attenuation n dB \*\*\* |

*Table 2 - GPIO bit mapping*

\* GPIO bits for antenna mode should be 0b110 (antennas connected, noise source off) but GPIO power-on default is 0b111 (antennas connected, noise source on). Since the RF switch has 60dB isolation this is unlikely to be an issue.

\*\* The ability to disable RF amplifier power was added to Mk3 FEMS after concerns about amplifier stability and strict site EMI controls. There is no ability to disable RF amplifiers in Mk1 and 2 FEMs.

\*\*\* PAM attenuator value bits (n) are inverted so that power on default is minimum attenuation.

The RF amplifier power can only be disabled with mk3 FEMs.

The magnetometer is not visible unless *pass-through* mode is enabled in the position sensor (IMU). By default it will not appear on I<sup>2</sup>C address scans.

### Serial numbers

- Each module has two serial numbers; a six character human-readable string stored in EEPROM and a 64 bit (8 byte) number stored in ROM. To avoid confusion we use *Serial Number* to refer to the human readable six characters stored in EEPROM and *Electronic Serial Number* or *ESN* to refer to the 64 bit number stored in factory programmed ROM.

- The EEPROMs (64k bit) are programmed in Cambridge. The first 6 bytes are programmed with the human-readable serial number in ASCII text, e.g. *FEM123*, *PAM123*.

- The PAM ESN is a 64 bit factory programmed ROM. In the FEM, the ESN is inside the temperature sensor IC.
