# -*- coding: utf-8 -*-
"""
HERA RF module tests
Requires Python 2.7

Authors: Tian, Steve
Cambridge 2018-2020
"""

from __future__ import print_function
import i2c_volt,i2c_bar,i2c_eeprom,i2c_motion,i2c_sn,i2c_temp,i2c_gpio,i2c
#import numpy as np,time,logging,struct,random,sys,argparse,json
import datetime,os,sys,subprocess,argparse,logging

logger = logging.getLogger(__name__)

p = argparse.ArgumentParser()
p.add_argument("-w", "--write", action='store_true', default=False, \
               help="write to file")
p.add_argument("-c", "--comment", required=False, help="optional comment")
args = vars(p.parse_args())

if sys.version_info >= (3,0):
    print("This program requires Python 2.7")
    sys.exit()

def db2gpio(ae,an):
    assert ae in range(0,16)
    assert an in range(0,16)
    ae = 15 - ae
    an = 15 - an
    val_str = '{0:08b}'.format((ae << 4) + an)
    val = int(val_str,2)
    return val

def gpio2db(val):
    assert val in range(0,256)
    val_str = '{0:08b}'.format(val)
    ae = int(val_str[0:4],2)
    an = int(val_str[4:8],2)
    return 15-ae, 15-an

def dc2dbm(val):
    assert val>=0 and val<=3.3, "Input value {} out range of 0-3.3V".format(val)
    slope = 27.31294863
    intercept = -55.15991678
    res = val * slope + intercept
    return res

ACCEL_ADDR = 0X69     #
MAG_ADDR = 0x0c       #
BAR_ADDR = 0x77       #
VOLT_PAM_ADDR = 0x36  # used by RF power measurement
ROM_FEM_ADDR = 0x51   #
ROM_PAM_ADDR = 0x52   #
TEMP_ADDR = 0x40      #
INA_ADDR_PAM = 0x44   #
INA_ADDR_FEM = 0x45   # Mk2 & Mk3 FEMs only
LTC_ADDR_FEM = 0X4f   # Mk1 FEM only
SN_ADDR = 0x50        #
GPIO_PAM_ADDR = 0x21  #
GPIO_FEM_ADDR = 0x20  #

ANT0_I2C_GPIO_SDA_PIN = 2  # Pi hardware SDA pin
ANT0_I2C_GPIO_SCL_PIN = 3  # Pi hardware SCL pin
ANT1_I2C_GPIO_SDA_PIN = 4
ANT1_I2C_GPIO_SCL_PIN = 14
ANT2_I2C_GPIO_SDA_PIN = 6
ANT2_I2C_GPIO_SCL_PIN = 12
ANT3_I2C_GPIO_SDA_PIN = 16
ANT3_I2C_GPIO_SCL_PIN = 26

# RPI I2C interface
# Port 0 - hardware i2c pins (2,3) on the Raspberry Pi
# Ports 1-3: Group of 3 FEM/PAMs using a SNAP
baud = 10000
i2cport = 0
i2clist = [[ANT0_I2C_GPIO_SDA_PIN,ANT0_I2C_GPIO_SCL_PIN],
          [ANT1_I2C_GPIO_SDA_PIN,ANT1_I2C_GPIO_SCL_PIN],
          [ANT2_I2C_GPIO_SDA_PIN,ANT2_I2C_GPIO_SCL_PIN],
          [ANT3_I2C_GPIO_SDA_PIN,ANT3_I2C_GPIO_SCL_PIN]]
bus = i2c.I2C_PIGPIO(i2clist[i2cport][0], i2clist[i2cport][1], baud)
femRev = "mk3"   # no need to adjust this, it will be tested
femID = "FEM"
pamID = "PAM"
femFound = False
pamFound = False
SAVE = False     # save to file flag

# ANSI escape characters for text colour
OKGREEN = '\033[92m'
WARNING = '\033[93m'
ENDC = '\033[0m'

# Save current stdout so that we can revert sys.stdout after completion
stdout_fileno = sys.stdout

### Try to read FEM serial number from EEPROM
rom = i2c_eeprom.EEP24XX64(bus,ROM_FEM_ADDR)
try:
    femrom = rom.readString()
    if femrom[0:3] == 'FEM':
        femID = femrom[0:6]
        femFound = True
except:
    pass

### Try to read PAM serial number from EEPROM
### Note this method reads up to the first null byte 0x00. If no NS data
### has been previously written and there is no null byte, this will read
### the whole 64kB address space which takes approx 10s
rom=i2c_eeprom.EEP24XX64(bus,ROM_PAM_ADDR)
try:
    pamrom = (rom.readString())
    if pamrom[0:3] == 'PAM':
        pamID = pamrom[0:6]
        pamFound = True
except:
    pass

# Redirect sys.stdout to the file
if (args['write']):
    f = open(femID+'_'+pamID+'_test.txt', 'w')
    print("Writing to file: "+femID+'_'+pamID+"_test.txt")
    SAVE = True
    sys.stdout = f

now = datetime.datetime.now()
print("\nDate:",now.strftime('%d %b %Y  %H:%M:%S'))
if (args['comment'] != None):
    c = args['comment']
else:
    c = ""
print("Comment:", c, "\n")

    
# Peform an i2c address space scan:
# we need Popen to capture the subprocess so we can save to file, if required
cmd = "i2cdetect -y 1"
p1 = subprocess.Popen(cmd, shell=True, stdin=None, stdout=subprocess.PIPE, \
                      stderr=subprocess.PIPE)
scan_output, err = p1.communicate()
print(scan_output)

if (femFound):
    print("Found:",femID)
else:
    print("FEM: no valid serial number found")

if (pamFound):
    print("Found:",pamID)
else:
    print("PAM: no valid serial number found")
print("")


### FEM TESTS
if (femFound):
    ### Try to read noise source data, Python dictionary format
    index1 = femrom.find("NS={")
    index2 = femrom.find("}")
    if index1 != 7 or index2 < 12:
        print("{0}: no valid noise data found".format(femID))
    else:
        print("{0}: noise source data:".format(femID))
        print("{0}: {1}".format(femID, femrom[index1+4:index2]))
    
    ### Read model number from temp & volt sensors, deduce FEM revision
    ### Mk3: temp sensor Si7021, volt sensor INA219 (0x45)
    ### Mk2: temp sensor Si7051, volt sensor INA219 (0x45)
    ### Mk1: temp sensor Si7051, volt sensor LTC2990 (0x4f)
    temp = i2c_temp.Si70XX(bus,TEMP_ADDR)
    tModel = temp.model()
    if (tModel == "Si7021"):
        print("{0}: temp sensor type {1}, Mk3 FEM".format(femID, tModel))
    elif (tModel == "Si7051"):
        print("{0}: temp sensor type {1}, Mk1/2 FEM".format(femID, tModel))
        femRev = "mk2"
    else:
        print("{0}: temp sensor type {1}, rev unknown".format(femID, tModel))
        femRev = "?"
    if (femRev != "mk3"):   # not revision Mk3, determine Mk1 or Mk2
        try:
            ltc=i2c_volt.LTC2990(bus,LTC_ADDR_FEM)
            ltc.init()
            ltc.readVolt('vcc')
            print("{0}: volt sensor type LTC2990, Mk1 FEM".format(femID))
            femRev = "mk1"
        except:
            print("{0}: volt sensor type INA219, Mk2 FEM".format(femID))
            femRev = "mk2"

    ### Try to read 64 bit Electronic Serial Number from temperature sensor
    if (femRev == "mk3"):
        temp = i2c_temp.Si7021(bus,TEMP_ADDR)
    else:
        temp = i2c_temp.Si7051(bus,TEMP_ADDR)
    sn=temp.sn()
    sernum = 0
    for b in sn:
        sernum = sernum * 256 + int(b)
    print("{0}: sensor ID: {1} ({2:.18})".format(femID, sernum, hex(sernum)))
    
    ### Read GPIO using mode switch, check 3 operational modes can be set
    ### FEM Mk3 compatibility: the payload is expanded from 3 to 5 bits to
    ### the RF sections remain powered on, bits 3-4 (E-N) set RF amp power
    gpio=i2c_gpio.PCF8574(bus,GPIO_FEM_ADDR)
    smode_decode = {0b11000:'load', 0b11001:'noise',0b11110:'antenna'}
    val=gpio.read()
    print("{0}: initial switch state (GPIO): {1}".format(femID, bin(val)))
    smode_bin = {0:0b11000,1:0b11001,2:0b11110}
    smode_txt = {0:'load',1:'noise', 2:'antenna'}
    for i in range(3):
        gpio.write(smode_bin[i])
        val=gpio.read()
        if val in smode_decode:
            femMode = (smode_decode[val])
        else:
            if SAVE:
                femMode = "Unknown"
            else:
                femMode = WARNING+"Unknown"+ENDC
        if femMode == smode_txt[i]:
            if SAVE:
                success = "Success"
            else:
                success = OKGREEN+"Success"+ENDC
        else:
            if SAVE:
                success = "Fail"
            else:
                success = WARNING+"Fail"+ENDC
        print("{0}: set switch (GPIO) state to {1}: {2}, {3}".format(femID, 
              smode_txt[i], bin(val), success))
    
    ### Try to read the temperature sensor
    if (femRev == "mk3"):
        t = temp.readTempRH()
        print("{0}: temperature: {1:.4}C".format(femID, t[0]))
        print("{0}: humidity: {1:.4}%".format(femID, t[1]))
    else:
        t = temp.readTemp()
        print("{0}: temperature: {1:.4}C".format(femID, t))
    
    ### Dual voltage sensor, try to read bus volts and shunt resistor (0.1 ohm)
    ### volts (for bus voltage and current)
    if ((femRev == "mk3") | (femRev == "mk2")):
        ina=i2c_volt.INA219(bus,INA_ADDR_FEM)
        ina.init()
        vbus = ina.readVolt('bus')
        vshunt = ina.readVolt('shunt')
        res = 0.1
        current = vshunt*1000/res
        if ((vbus >= 5.5) & (vbus < 8) & (current > 410) & (current < 490)):
            if SAVE:                          # use colour only on screen
                isOK = ", OK"
            else:
                isOK = ", "+OKGREEN+"OK"+ENDC
        else:
            isOK = ""
        print("{0}: supply sensor: {1}V, {2}mA{3}".format(femID,vbus,current,isOK))
    elif (femRev == "mk1"):
        ltc=i2c_volt.LTC2990(bus,LTC_ADDR_FEM)
        ltc.init()
        vbus = ltc.readVolt('v1')
        # problems reading Mk1 voltage sensor, skip print of results
    
    ### Try to read orientation from IMU sensor
    imu = i2c_motion.IMUSimple(bus,ACCEL_ADDR,orient=[[0,0,1],[1,1,0],[-1,1,0]])
    imu.init()
    theta, phi = imu.pose
    print("{0}: position sensor: theta={1:.5}, phi={2:.5}".format(femID,theta,phi))
    imu.mpu.powerOff()
    
    ### Try to read air pressure from the Barometric Pressure Sensor. Altitude is
    ### calculated by reference to the International Standard Atmosphere,
    ### 1013.25 hPa at mean sea level (pilots will be familiar with this)
    bar = i2c_bar.MS5611_01B(bus,BAR_ADDR)
    bar.init()
    rawt,dt = bar.readTemp(raw=True)
    press = bar.readPress(rawt,dt)
    alt = bar.toAltitude(press,rawt/100.)
    print('{0}: barometer: pressure={1:.5}hPa, altitude={2:.4}m'
          .format(femID,press,alt))
    print("")


### PAM TESTS
if (pamFound):
    ### Try to read 48 bit Electronic Serial Number (64 bit ROM)
    sn=i2c_sn.DS28CM00(bus,SN_ADDR)
    val=sn.readSN()
    sernum = 0
    for b in val:
        sernum = sernum * 256 + int(b)
    print("{0}: ROM ID: {1} ({2:.18})".format(pamID, sernum, hex(sernum)))
    
    ### Read initial attenuator settings & try set a new value, twice
    gpio=i2c_gpio.PCF8574(bus,GPIO_PAM_ADDR)
    val=gpio.read()
    ve,vn=gpio2db(val)
    print("{0}: initial attenuator settings: {1}dB(E), {2}dB(N)"
          .format(pamID,ve,vn))
    atten = 10     # Attempt1: try changing attenuator to <atten> dB
    ve=int(atten)
    vn=int(atten)
    gpio.write(db2gpio(ve,vn))
    val=gpio.read()
    ve,vn=gpio2db(val)
    ve_res = "Error (E)"
    vn_res = "Error (N)"
    if ve == atten:
        if SAVE:       # colourise to screen only
            ve_res = "Success (E)"
        else:
            ve_res = OKGREEN+"Success (E)"+ENDC
    elif not SAVE:
        ve_res = WARNING+"Error (E)"+ENDC
    if vn == atten:
        if SAVE:
            vn_res = "Success (N)"
        else:
            vn_res = OKGREEN+"Success (N)"+ENDC
    elif not SAVE:
        vn_res = WARNING+"Error (N)"+ENDC
    print("{0}: trying attenuator setting {1}dB: {2}, {3}"
          .format(pamID, atten, ve_res, vn_res))
    atten = 0     # Attempt2: try changing attenuator to <atten> dB
    ve=int(atten)
    vn=int(atten)
    gpio.write(db2gpio(ve,vn))
    val=gpio.read()
    ve,vn=gpio2db(val)
    ve_res = "Error (E)"
    vn_res = "Error (N)"
    if ve == atten:
        if SAVE:       # colourise to screen only
            ve_res = "Success (E)"
        else:
            ve_res = OKGREEN+"Success (E)"+ENDC
    elif not SAVE:
        ve_res = WARNING+"Error (E)"+ENDC
    if vn == atten:
        if SAVE:
            vn_res = "Success (N)"
        else:
            vn_res = OKGREEN+"Success (N)"+ENDC
    elif not SAVE:
        vn_res = WARNING+"Error (N)"+ENDC
    print("{0}: trying attenuator setting {1}dB: {2}, {3}"
          .format(pamID, atten, ve_res, vn_res))
    
    ### Dual voltage sensor, try to read bus volts and shunt resistor (0.1 ohm)
    ### volts (for bus voltage and current)
    ina=i2c_volt.INA219(bus,INA_ADDR_PAM)
    ina.init()
    vshunt = ina.readVolt('shunt')
    vbus = ina.readVolt('bus')
    res = 0.1
    print("{0}: supply sensor: {1}V, {2}mA".format(pamID, vbus, vshunt*1000/res))
    
    ### Try to read ADC for the RF true power sensor.
    ### Note the loss offset to account for coupler loss
    volt=i2c_volt.MAX11644(bus,VOLT_PAM_ADDR)
    volt.init()
    vp1,vp2=volt.readVolt()
    loss = 9.8
    print("{0}: RF power: {1:.4}dBm(E), {2:.4}dBm(N)"
          .format(pamID, dc2dbm(vp1)+loss, dc2dbm(vp2)+loss))

# Close the file, if open
if (args['write']):
    sys.stdout.close()
    # restore sys.stdout to the previously saved file handler
    sys.stdout = stdout_fileno
    print("Done.")
    #cmd = "cat "femID+'_'+pamID+"_test.txt"
    os.system("cat "+femID+'_'+pamID+"_test.txt")
