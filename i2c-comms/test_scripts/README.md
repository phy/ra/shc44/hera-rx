## Test scripts

Python scripts for I<sup>2</sup>C device testing and FEM commissioning tests.

This uses *[pigpio](http://abyz.me.uk/rpi/pigpio/)* module for I<sup>2</sup>C access where the clock speed is set by by *pigpio*. There is no requirement to set speed in */boot/config.txt*.

The modules were written in 2018 for Python 2.7 on a FPGA, then ported to a Raspberry Pi 2 running *Raspberry Pi OS Lite*. Since Python 2.7 was deprecated on 1 January 2020, finding all the required dependencies (e.g. pip2) can be challenging.

The module *i2c.py*, used by other modules for I<sup>2</sup>C communication, contains substational code for the FPGA implementation therefore porting these scripts to Python 3 is also challenging and unlikely to be worthwhile this late in the project..?

### Setup

Connect the Pi, CANbus adapter and RF module(s) as per the [EEPROM write scripts](../eeprom/README.md).

Requires the *[pigpio](http://abyz.me.uk/rpi/pigpio/)* daemon to be running:

    sudo apt install pigpio
    sudo systemctl enable pigpiod   # enable auto-start on boot
    sudo systemctl start pigpiod


Requires the *crcmod* module:

    pip install crcmod    # used to check CRC from ROM devices

### Usage

*python full-tests.py [-h] [-w] [-c "any comment to include in the output"]*

Note:

This tests a FEM, PAM, or both. It can also indicate the FEM revision (mark 1, 2, or 3). The only exception handling is whether a FEM and/or PAM is connected. The script will gracefully skip an attempt to test an absent FEM or PAM.

If a module is connected and contains a faulty I<sup>2</sup>C device, the script will exit with an exception. The exception message will indicate the faulty device.

Example:  
*python full-tests.py -w -c "test with 500m cat7"*  
writes test output to disk (automatically generated filename) with comment.

### Ackowledgement

Thanks to Tian Huang for his excellent Python I<sup>2</sup>C device modules for the Pi. These were written in 2018-19 while Tian was working as a Postdoc in the Astrophysics group at Cambridge.
