# -*- coding: utf-8 -*-
"""
HERA RF module GPIO tests
Requires Python 2.7

Authors: Tian, Steve
Cambridge 2018-2021
"""

from __future__ import print_function
#import i2c_volt,i2c_bar,i2c_eeprom,i2c_motion,i2c_sn,i2c_temp,i2c_gpio,i2c
#import numpy as np,time,logging,struct,random,sys,argparse,json
import i2c_eeprom,i2c_gpio,i2c
import datetime,sys,subprocess,argparse,logging

logger = logging.getLogger(__name__)

p = argparse.ArgumentParser()
p.add_argument("-l", "--load", action="store_true", 
               help="switch to load (0b11000)")
p.add_argument("-n", "--noise", action="store_true", 
               help="switch to noise source (0b11001)")
p.add_argument("-a", "--antenna", action="store_true", 
               help="switch to antenna (0b11110)")
p.add_argument("-d", "--default", action="store_true", 
               help="power on default (0xFF)")
p.add_argument("-o", "--off", action="store_true", 
               help="all off (0x00)")
p.add_argument("-0", action="store_true", 
               help="noise source power bit off")
p.add_argument("-1", action="store_true", 
               help="noise source power bit on")
args = vars(p.parse_args())

if sys.version_info >= (3,0):
    print("This program requires Python 2.7")
    sys.exit()

def db2gpio(ae,an):
    assert ae in range(0,16)
    assert an in range(0,16)
    ae = 15 - ae
    an = 15 - an
    val_str = '{0:08b}'.format((ae << 4) + an)
    val = int(val_str,2)
    return val

def gpio2db(val):
    assert val in range(0,256)
    val_str = '{0:08b}'.format(val)
    ae = int(val_str[0:4],2)
    an = int(val_str[4:8],2)
    return 15-ae, 15-an

ACCEL_ADDR = 0X69     #
MAG_ADDR = 0x0c       #
BAR_ADDR = 0x77       #
VOLT_PAM_ADDR = 0x36  # used by RF power measurement
ROM_FEM_ADDR = 0x51   #
ROM_PAM_ADDR = 0x52   #
TEMP_ADDR = 0x40      #
INA_ADDR_PAM = 0x44   #
INA_ADDR_FEM = 0x45   # Mk2 & Mk3 FEMs only
LTC_ADDR_FEM = 0X4f   # Mk1 FEM only
SN_ADDR = 0x50        #
GPIO_PAM_ADDR = 0x21  #
GPIO_FEM_ADDR = 0x20  #

ANT0_I2C_GPIO_SDA_PIN = 2  # Pi hardware SDA pin
ANT0_I2C_GPIO_SCL_PIN = 3  # Pi hardware SCL pin
ANT1_I2C_GPIO_SDA_PIN = 4
ANT1_I2C_GPIO_SCL_PIN = 14
ANT2_I2C_GPIO_SDA_PIN = 6
ANT2_I2C_GPIO_SCL_PIN = 12
ANT3_I2C_GPIO_SDA_PIN = 16
ANT3_I2C_GPIO_SCL_PIN = 26

# RPI I2C interface
# Port 0 - hardware i2c pins (2,3) on the Raspberry Pi
# Ports 1-3: Group of 3 FEM/PAMs using a SNAP
baud = 10000
i2cport = 0
i2clist = [[ANT0_I2C_GPIO_SDA_PIN,ANT0_I2C_GPIO_SCL_PIN],
          [ANT1_I2C_GPIO_SDA_PIN,ANT1_I2C_GPIO_SCL_PIN],
          [ANT2_I2C_GPIO_SDA_PIN,ANT2_I2C_GPIO_SCL_PIN],
          [ANT3_I2C_GPIO_SDA_PIN,ANT3_I2C_GPIO_SCL_PIN]]
bus = i2c.I2C_PIGPIO(i2clist[i2cport][0], i2clist[i2cport][1], baud)
femRev = "mk3"   # no need to adjust this, it will be tested
femID = "FEM"
pamID = "PAM"
femFound = False
pamFound = False
SAVE = False     # save to file flag

# ANSI escape characters for text colour
OKGREEN = '\033[92m'
WARNING = '\033[93m'
ENDC = '\033[0m'

# Save current stdout so that we can revert sys.stdout after completion
stdout_fileno = sys.stdout

### Try to read FEM serial number from EEPROM
rom = i2c_eeprom.EEP24XX64(bus,ROM_FEM_ADDR)
try:
    femrom = rom.readString()
    if femrom[0:3] == 'FEM':
        femID = femrom[0:6]
        femFound = True
except:
    pass

### Try to read PAM serial number from EEPROM
### Note this method reads up to the first null byte 0x00. If no NS data
### has been previously written and there is no null byte, this will read
### the whole 64kB address space which takes approx 10s
rom=i2c_eeprom.EEP24XX64(bus,ROM_PAM_ADDR)
try:
    pamrom = (rom.readString())
    if pamrom[0:3] == 'PAM':
        pamID = pamrom[0:6]
        pamFound = True
except:
    pass

now = datetime.datetime.now()
print("\nDate:",now.strftime('%d %b %Y  %H:%M:%S'))
    
# Peform an i2c address space scan:
# we need Popen to capture the subprocess so we can save to file, if required
#cmd = "i2cdetect -y 1"
#p1 = subprocess.Popen(cmd, shell=True, stdin=None, stdout=subprocess.PIPE, \
#                      stderr=subprocess.PIPE)
#scan_output, err = p1.communicate()
#print(scan_output)

if (femFound):
    print("Found:",femID)
elif (pamFound):
    print("Found:",pamID)
else:
    print("No FEM/PAM found")


### FEM GPIO
if (femFound):
    ### Read GPIO using mode switch, check 3 operational modes can be set
    ### FEM Mk3 compatibility: the payload is expanded from 3 to 5 bits to
    ### the RF sections remain powered on, bits 3-4 (E-N) set RF amp power
    gpio=i2c_gpio.PCF8574(bus,GPIO_FEM_ADDR)
    i = -1      # index for GPIO settings dictionary
    smode_decode = {0b11000:'load',0b11001:'noise',0b11110:'antenna', \
        0b11111:'default', 0b0:'off'}
    smode_bin = {0:0b11000,1:0b11001,2:0b11110,3:0b11111,4:0b0}
    smode_txt = {0:'load',1:'noise',2:'antenna',3:'default',4:'off', \
                 5:'noise power off',6:'noise power on'}
    val=gpio.read()
    print("{0} initial GPIO state: {1}".format(femID, bin(val)))
    if (args['load']):
        i = 0
        gpio.write(smode_bin[i])
    elif (args['noise']):
        i = 1
        gpio.write(smode_bin[i])
    elif (args['antenna']):
        i = 2
        gpio.write(smode_bin[i])
    elif (args['default']):
        i = 3
        gpio.write(smode_bin[i])
    elif (args['off']):
        i = 4
        gpio.write(smode_bin[i])
    elif (args['0']):
        i = 5
        val=gpio.read()       # read GPIO register & set noise bit low
        gpio.write(val & 0b11111110)
    elif (args['1']):
        i = 6
        val=gpio.read()       # read GPIO register & set noise bit high
        gpio.write(val | 0b00000001)
    if (i < 0):
            print("{0} GPIO unchanged".format(femID))
    else:
        val=gpio.read()
        print("{0} new GPIO state: {1} {2}".format(femID, 
              OKGREEN + smode_txt[i] + ENDC, bin(val)))
            

### PAM GPIO
if (pamFound):
    ### Read initial attenuator settings
    gpio=i2c_gpio.PCF8574(bus,GPIO_PAM_ADDR)
    val=gpio.read()
    ve,vn=gpio2db(val)
    print("{0}: initial attenuator settings: {1}dB(E), {2}dB(N)"
          .format(pamID,ve,vn))

print("")
