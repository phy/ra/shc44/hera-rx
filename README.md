## CANbus

Details of the adapter required for I<sup>2</sup>C testing and programming.

## Hardware

RF system enclosures, FEM power supply. Does not include FEM/PAM schematics.

## I<sup>2</sup>C Comms

Scripts for initial programming and testing (requires the CANbus adapter).

## Test results

- **I<sup>2</sup>C:** test results include test/fail indications, FEM version, supply currents etc.

- **RF:** 3 port VNA measurement files in Touchstone format for the RF chain; FEM, 500m fibre, PAM.